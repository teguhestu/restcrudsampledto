/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.dto.validation;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author teguh.estu
 */
public class DateFormatRangeValidator implements ConstraintValidator<DateFormatRange, Object>{
    private String start;
    private String end;
    private String format;
    
    public void initialize(DateFormatRange validateDateRange) {
        start = validateDateRange.start();
        end =  validateDateRange.end();
        format = validateDateRange.format();
    }

    public boolean isValid(Object object,
                           ConstraintValidatorContext constraintValidatorContext) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            Class clazz = object.getClass();
            String startDate = null;
            Date dStartDate = null;
            Method startGetter = clazz.getMethod(getAccessorMethodName(start), new Class[0]);
            Object startGetterResult = startGetter.invoke(object, null);
            if (startGetterResult != null && startGetterResult instanceof String && !"".equals(startGetterResult) ){
                startDate = (String) startGetterResult;
                dStartDate = formatter.parse(startDate);
            }else{
                return true;
            }
            String endDate = null;
            Date dEndDate = null;
            Method endGetter = clazz.getMethod(getAccessorMethodName(end), new Class[0]);
            Object endGetterResult = endGetter.invoke(object, null);
            if (endGetterResult == null || "".equals(startGetterResult)){
                return true;
            }
            if (endGetterResult instanceof String){
                endDate = (String) endGetterResult;
                dEndDate = formatter.parse(endDate);
            }
            return (dStartDate.before(dEndDate) || dStartDate.equals(dEndDate));           
        } catch (Throwable e) {
            System.err.println(e);
        }

        return true;
    }
    private String getAccessorMethodName(String property){
        StringBuilder builder = new StringBuilder("get");
        builder.append(Character.toUpperCase(property.charAt(0))); 
        builder.append(property.substring(1));
        return builder.toString();
    }
}
