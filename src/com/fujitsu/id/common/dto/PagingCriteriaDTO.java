/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author teguhpa
 */
public class PagingCriteriaDTO<T extends SearchDTO> {
    private Integer currentPage;
    private Integer rowsPerPage;
    private Integer startRow;
    private Integer endRow;
    private List<T> searchDTO = new ArrayList<T>();

    public PagingCriteriaDTO(Integer currentPage, Integer rowsPerPage) {
        this.currentPage = currentPage;
        this.rowsPerPage = rowsPerPage;
        startRow = ((currentPage - 1) * rowsPerPage);
        endRow = startRow + rowsPerPage;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public Integer getEndRow() {
        return endRow;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(Integer rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public List<T> getSearchDTO() {
        return searchDTO;
    }

    public void setSearchDTO(List<T> searchDTO) {
        this.searchDTO = searchDTO;
    }
    
    
}
