/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author teguhpa
 */
public class PagingResultDTO<T extends BaseDTO> extends BaseDTO {
    private Integer currentPage;
    private Integer rowsPerPage;
    private Integer totalRecords;
    private Integer totalPage;
    private Integer startRow;
    private List<T> dto = new ArrayList<T>();

    public PagingResultDTO(Integer currentPage, Integer rowsPerPage) {
        this.currentPage = currentPage;
        this.rowsPerPage = rowsPerPage;
        this.startRow = (currentPage - 1) * rowsPerPage;
    }

    public Integer getStartRow() {
        return startRow;
    }
    
    
    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(Integer rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getDto() {
        return dto;
    }

    public void setDto(List<T> dto) {
        this.dto = dto;
    }

    public void setStartRow(Integer startRow) {
        this.startRow = startRow;
    }
    
    
}
