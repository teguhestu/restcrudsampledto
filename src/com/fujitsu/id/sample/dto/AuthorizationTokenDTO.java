/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.dto;

import java.util.Date;
import java.util.UUID;

/**
 *
 * @author teguh.estu
 */
public class AuthorizationTokenDTO {
    
    private final static Integer DEFAULT_TIME_TO_LIVE_IN_SECONDS = (60 * 60 * 24); //1 Days

    private String token;
    private String username;
    private Date createdDate;
    private Date expiryDate;

    public AuthorizationTokenDTO() {
    }
    
    public AuthorizationTokenDTO(String username) {
        this.username = username;
        this.token = UUID.randomUUID().toString();
        //this.user = user;
        this.createdDate = new Date();
        this.expiryDate = new Date(System.currentTimeMillis() + (DEFAULT_TIME_TO_LIVE_IN_SECONDS * 1000L));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    
}
