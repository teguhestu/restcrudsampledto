/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.sample.dto;

import com.fujitsu.id.common.dto.EntityDTO;
import com.fujitsu.id.common.dto.validation.group.CreateGroup;
import com.fujitsu.id.common.dto.validation.group.DeleteGroup;
import com.fujitsu.id.common.dto.validation.group.UpdateGroup;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author teguhpa
 */
public class EmployeeDTO extends EntityDTO {
    private Integer empNo;
    private Date birthDate;
    private String firstName;
    private String lastName;
    private String gender;
    private Date hireDate;

   
    @NotNull(message = "{message.notnull} @Param=[aaa,bbb,ccc]",
            groups = {CreateGroup.class, UpdateGroup.class, DeleteGroup.class})
   // @Pattern(regexp = "[ddddd]",groups = {CreateGroup.class, UpdateGroup.class, DeleteGroup.class} )
    public Integer getEmpNo() {
        return empNo;
    }

    public void setEmpNo(Integer empNo) {
        this.empNo = empNo;
    }

    @NotNull(groups = {CreateGroup.class, UpdateGroup.class})
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

     @NotNull(message = "{message.notnull}",
             groups = {CreateGroup.class, UpdateGroup.class})
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

      @NotNull(message = "{message.notnull}",
              groups = {CreateGroup.class, UpdateGroup.class})
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

     @NotNull(groups = {CreateGroup.class, UpdateGroup.class})
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

     @NotNull(groups = {CreateGroup.class, UpdateGroup.class})
     
    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    
}
