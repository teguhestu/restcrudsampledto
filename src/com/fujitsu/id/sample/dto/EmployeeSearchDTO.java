/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.sample.dto;

import com.fujitsu.id.common.dto.SearchDTO;

/**
 *
 * @author teguhpa
 */
//@javax.validation.constraints.
public class EmployeeSearchDTO extends SearchDTO {
    private Integer empNo;
    private String lastName;
    private String firstName;

    public Integer getEmpNo() {
        return empNo;
    }

    public void setEmpNo(Integer empNo) {
        this.empNo = empNo;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    
}
