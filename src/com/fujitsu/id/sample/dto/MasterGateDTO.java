/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.dto;

import com.fujitsu.id.common.dto.Constant;
import com.fujitsu.id.common.dto.EntityDTO;
import com.fujitsu.id.common.dto.validation.DateFormat;
import com.fujitsu.id.common.dto.validation.DateFormatRange;
import com.fujitsu.id.common.dto.validation.group.CreateGroup;
import com.fujitsu.id.common.dto.validation.group.DeleteGroup;
import com.fujitsu.id.common.dto.validation.group.UpdateGroup;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author teguh.estu
 */
@DateFormatRange(start = "dbegineff", end = "dendeff", format = Constant.DATE_FORMAT,
                 message = "Begin Effective Date should be less than or equal to End Effective Date",
                 groups = {CreateGroup.class, UpdateGroup.class})
public class MasterGateDTO extends EntityDTO {
    private String vgateid;
    private String vgatedesc;
    private String vcrea;
    private String dcrea;
    private String vmodi;
    private String dmodi;
    private String mstorVslocid;
    private String mstorMplantVplantid;
    private String dbegineff;
    private String dendeff;

    @NotBlank(message = "Gate ID should not be empty", groups = {CreateGroup.class, UpdateGroup.class, DeleteGroup.class})
    @Size(max = 8, message = "Gate ID length should be less than or equal to 8", groups = {CreateGroup.class, UpdateGroup.class})
    public String getVgateid() {
        return vgateid;
    }

    public void setVgateid(String vgateid) {
        this.vgateid = vgateid;
    }

    @NotBlank(message = "Gate Description should not be empty", groups = {CreateGroup.class, UpdateGroup.class})
    @Size(max = 40, message = "Gate Description length should be less than or equal to 40",groups = {CreateGroup.class, UpdateGroup.class})
    public String getVgatedesc() {
        return vgatedesc;
    }

    public void setVgatedesc(String vgatedesc) {
        this.vgatedesc = vgatedesc;
    }

    @NotBlank(message = "Created User should not be empty", groups = {CreateGroup.class})
    @Size(max = 12, message = "Created User length should be less than or equal to 12", groups = {CreateGroup.class})
    public String getVcrea() {
        return vcrea;
    }

    public void setVcrea(String vcrea) {
        this.vcrea = vcrea;
    }

    public String getDcrea() {
        return dcrea;
    }
    
    public void setDcrea(String dcrea) {
        this.dcrea = dcrea;
    }

    @NotBlank(message = "Modified User should not be empty", groups = {UpdateGroup.class})
    @Size(max = 12, message = "Modified User length should be less than or equal to 12", groups = {CreateGroup.class, UpdateGroup.class})
    public String getVmodi() {
        return vmodi;
    }

    public void setVmodi(String vmodi) {
        this.vmodi = vmodi;
    }

    public String getDmodi() {
        return dmodi;
    }

    public void setDmodi(String dmodi) {
        this.dmodi = dmodi;
    }

    @NotBlank(message = "Location ID should not be empty", groups = {CreateGroup.class, UpdateGroup.class})
    @Size(max = 4, message = "Location ID length should be less than or equal to 4", groups = {CreateGroup.class, UpdateGroup.class})
    public String getMstorVslocid() {
        return mstorVslocid;
    }

    public void setMstorVslocid(String mstorVslocid) {
        this.mstorVslocid = mstorVslocid;
    }

    @NotBlank(message = "Plant ID should not be empty", groups = {CreateGroup.class, UpdateGroup.class})
    @Size(max = 4, message = "Plant ID length should be less than or equal to 4", groups = {CreateGroup.class, UpdateGroup.class})
    public String getMstorMplantVplantid() {
        return mstorMplantVplantid;
    }

    public void setMstorMplantVplantid(String mstorMplantVplantid) {
        this.mstorMplantVplantid = mstorMplantVplantid;
    }

    @NotBlank(message = "Begin Efective Date should not be empty", groups = {CreateGroup.class, UpdateGroup.class})
    @DateFormat(value = Constant.DATE_FORMAT, message = "Invalid format of Begin Effective Date", groups = {CreateGroup.class, UpdateGroup.class}) 
    public String getDbegineff() {
        return dbegineff;
    }

    public void setDbegineff(String dbegineff) {
        this.dbegineff = dbegineff;
    }

    @NotBlank(message = "End Effective Date should not be empty", groups = {CreateGroup.class, UpdateGroup.class})
    @DateFormat(value = Constant.DATE_FORMAT, message = "Invalid format of End Effective Date", groups = {CreateGroup.class, UpdateGroup.class}) 
    public String getDendeff() {
        return dendeff;
    }

    public void setDendeff(String dendeff) {
        this.dendeff = dendeff;
    }
    
    
}
