/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.dto;

import com.fujitsu.id.common.dto.SearchDTO;

/**
 *
 * @author teguh.estu
 */
public class MasterGateSearchDTO extends SearchDTO {
    private String vgateid;
    private String status;
    private String mstorMplantVplantid;
    private String mstorVslocid;

    public String getMstorVslocid() {
        return mstorVslocid;
    }

    public void setMstorVslocid(String mstorVslocid) {
        this.mstorVslocid = mstorVslocid;
    }
    
    
    public String getVgateid() {
        return vgateid;
    }

    public void setVgateid(String vgateid) {
        this.vgateid = vgateid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMstorMplantVplantid() {
        return mstorMplantVplantid;
    }

    public void setMstorMplantVplantid(String mstorMplantVplantid) {
        this.mstorMplantVplantid = mstorMplantVplantid;
    }
    
    
}
